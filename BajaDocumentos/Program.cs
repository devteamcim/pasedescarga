﻿using System;
using System.Collections.Generic;

namespace BajaDocumentos
{
    class Program
    {
        static void Main(string[] args)
        {
            Codigo.cArchivos oArch = new Codigo.cArchivos();
            int iCount = 0;

            if (args.Length > 0)
            {
                /*
                 * Esta forma permite procesar destinos diferentes de los definidos como parámetros
                 * 
                 * La forma de invocar esta parte es:
                 * 
                 *      BajaDocumentos "\\Servidor1\Destino1" "\\Servidor2\Destino2"
                 *      
                 */
                iCount = oArch.Baja(new List<string>(args));
            }
            else
            {
                /*
                 * Esta forma es para procesar solo los destinos definidos en la Base de Datos
                 * 
                 * La forma de invocar esta parte es:
                 * 
                 *      BajaDocumentos
                 *      
                 */
                iCount = oArch.Baja();
            }

            if (oArch.Valido)
            {
                Console.WriteLine(String.Format("Se descargaron {0} archivo(s) en {1} destino(s) diferente(s)", iCount, oArch.Destinos));
            }
            else
            {
                Console.WriteLine(String.Format("Ocurrió error: {0}", oArch.Error));
            }
        }
    }
}
