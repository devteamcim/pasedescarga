﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BajaDocumentos.Codigo
{
    class cMail
    {
        private string psError;
        private bool pbValido;

        public string Error
        {
            get
            {
                return psError;
            }
        }
        public bool Valido
        {
            get
            {
                return pbValido;
            }
        }
        private class rParametros
        {
            public string Servidor
            {
                get;
                set;
            }
            public string Usuario
            {
                get;
                set;
            }
            public string Contraseña
            {
                get;
                set;
            }
            public int Puerto
            {
                get;
                set;
            }
            public bool SSL
            {
                get;
                set;
            }
            public string Destinatarios
            {
                get;
                set;
            }
        }

        private rParametros GetParametros()
        {
            // Obtenemos los parámetros de correo
            rParametros rPar = new rParametros();

            using (SqlConnection oDB = new SqlConnection(Properties.Settings.Default.Conexion))
            {
                if (oDB.State == ConnectionState.Closed)
                    oDB.Open();
                using (SqlCommand oQry = new SqlCommand("[ibd].[usp_RecuperaMail]", oDB))
                {
                    oQry.CommandType = CommandType.StoredProcedure;
                    using (SqlDataReader rDatos = oQry.ExecuteReader())
                    {
                        if (rDatos.Read())
                        {
                            rPar.Servidor = rDatos.IsDBNull(rDatos.GetOrdinal("ServidorOrigen")) ? "" : rDatos.GetString(rDatos.GetOrdinal("ServidorOrigen"));
                            rPar.Usuario = rDatos.IsDBNull(rDatos.GetOrdinal("DireccionMail")) ? "" : rDatos.GetString(rDatos.GetOrdinal("DireccionMail"));
                            rPar.Contraseña = rDatos.IsDBNull(rDatos.GetOrdinal("Contraseña")) ? "" : rDatos.GetString(rDatos.GetOrdinal("Contraseña"));
                            rPar.Puerto = rDatos.IsDBNull(rDatos.GetOrdinal("Puerto")) ? 25 : rDatos.GetInt32(rDatos.GetOrdinal("Puerto"));
                            rPar.SSL = rDatos.IsDBNull(rDatos.GetOrdinal("SSL")) ? false : (rDatos.GetString(rDatos.GetOrdinal("SSL")).ToUpper() == "SI" ? true : false);
                            rPar.Destinatarios = rDatos.IsDBNull(rDatos.GetOrdinal("DestinoMail")) ? "" : rDatos.GetString(rDatos.GetOrdinal("DestinoMail"));
                        }
                    }
                }
            }

            return rPar;
        }
        private bool SendMail(string sSubject, string sBody)
        {
            // Enviamos el correo solicitado
            rParametros rPar = GetParametros();
            MailMessage oMail = new MailMessage();
            SmtpClient oSMTP = new SmtpClient();
            string[] aDestinos = rPar.Destinatarios.Split(';');

            try
            {
                oMail.Subject = sSubject;
                oMail.Body = sBody;
                oMail.IsBodyHtml = true;
                oMail.From = new MailAddress(rPar.Usuario);

                foreach (string sMail in aDestinos)
                {
                    string sCorreo = sMail.Trim();
                    if (sCorreo.IndexOf("@") > 0)
                        oMail.To.Add(sCorreo);                              // Solo agregamos direcciones de correo con formato adecuado
                }

                oSMTP.Host = rPar.Servidor;
                oSMTP.Port = rPar.Puerto;
                oSMTP.EnableSsl = rPar.SSL;
                oSMTP.Credentials = new NetworkCredential(rPar.Usuario, rPar.Contraseña);
                oSMTP.Send(oMail);

                pbValido = true;
            }
            catch (Exception ex)
            {
                psError = ex.Message.ToString();
                pbValido = false;
            }

            return pbValido;
        }

        public bool EnviaCorreo(string sAsunto, string sDestino, List<cArchivos.rDoc> aDocs)
        {
            StringBuilder sCuerpo = new StringBuilder();

            sCuerpo.Append(String.Format("Se enviaron existosamente a <strong>{0}</strong> los siguientes archivos:<br /><br /><ul>", sDestino));

            foreach (cArchivos.rDoc rReg in aDocs)
            {
                sCuerpo.Append(String.Format("<li type='circle'>{0}</li>", rReg.Nombre));
            }

            sCuerpo.Append("</ul>");

            return SendMail(sAsunto, sCuerpo.ToString());
        }
    }
}
