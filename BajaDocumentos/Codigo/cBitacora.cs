﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BajaDocumentos.Codigo
{
    class cBitacora
    {
        private string psError;
        private bool pbValido;

        public string Error
        {
            get
            {
                return psError;
            }
        }

        public bool Valido
        {
            get
            {
                return pbValido;
            }
        }

        public int AgregaBitacora(int Error, string Programa, string Origen, string Destino, string Archivo, string Descripcion)
        {
            int iRes = 0;

            using (SqlConnection oDB = new SqlConnection(Properties.Settings.Default.Conexion))
            {
                if (oDB.State == ConnectionState.Closed)
                    oDB.Open();
                using (SqlCommand oQry = new SqlCommand("[ibd].[usp_InsertBitacoraDocumentos]", oDB))
                {
                    oQry.CommandType = CommandType.StoredProcedure;
                    oQry.Parameters.AddWithValue("@Descripcion", Descripcion);
                    oQry.Parameters.AddWithValue("@Programa", Programa);
                    oQry.Parameters.AddWithValue("@Error", Error);
                    oQry.Parameters.AddWithValue("@Origen", Origen);
                    oQry.Parameters.AddWithValue("@Destino", Destino);
                    oQry.Parameters.AddWithValue("@Archivo", Archivo);

                    iRes = oQry.ExecuteNonQuery();
                }
            }
            return iRes;
        }
    }

}
