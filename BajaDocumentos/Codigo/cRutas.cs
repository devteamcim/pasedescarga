﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace BajaDocumentos.Codigo
{
    class cRutas
    {
        public class Ruta
        {
            public int Numero
            {
                get;
                set;
            }
            public string Destino
            {
                get;
                set;
            }
            public int Prioridad
            {
                get;
                set;
            }
        }

        public List<string> Rutas()
        {
            List<string> aRutas = new List<string>();

            using (SqlConnection oDB = new SqlConnection(Properties.Settings.Default.Conexion))
            {
                if (oDB.State == ConnectionState.Closed)
                    oDB.Open();
                using (SqlCommand oQry = new SqlCommand("[ibd].[usp_spsRutasDestinoPaseDocumentos]", oDB))
                {
                    oQry.CommandType = CommandType.StoredProcedure;
                    using (SqlDataReader rDatos = oQry.ExecuteReader())
                    {
                        while (rDatos.Read())
                        {
                            Ruta rRuta = new Ruta();
                            rRuta.Numero = rDatos.IsDBNull(rDatos.GetOrdinal("rtdNumero")) ? 0 : rDatos.GetInt32(rDatos.GetOrdinal("rtdNumero"));
                            rRuta.Destino = rDatos.IsDBNull(rDatos.GetOrdinal("rtdRuta")) ? "" : rDatos.GetString(rDatos.GetOrdinal("rtdRuta"));
                            rRuta.Prioridad = rDatos.IsDBNull(rDatos.GetOrdinal("rtdPrioridad")) ? 0 : rDatos.GetInt32(rDatos.GetOrdinal("rtdPrioridad"));

                            if (!String.IsNullOrEmpty(rRuta.Destino))
                            {
                                aRutas.Add(rRuta.Destino);
                            }
                        }
                    }
                }
            }
            return aRutas;
        }
    }
}
