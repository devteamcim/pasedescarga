﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace BajaDocumentos.Codigo
{
    class cArchivos
    {
        public class rDoc
        {
            public int Indice
            {
                get;
                set;
            }
            public int ID
            {
                get;
                set;
            }
            public DateTime Fecha
            {
                get;
                set;
            }
            public string Original
            {
                get;
                set;
            }
            public string Nombre
            {
                get;
                set;
            }
            public string Tipo
            {
                get;
                set;
            }
            public int Estatus
            {
                get;
                set;
            }

            public byte[] Contenido
            {
                get;
                set;
            }
        }

        private int piCount;                                                // Número de archivos procesados
        private string psError;                                             // Descripción del posible error
        private bool pbValido;                                              // Estatus final de la última función
        private int piDestinos;                                             // Número de destinos procesados

        public string Error
        {
            get
            {
                return psError;
            }
        }

        public bool Valido
        {
            get
            {
                return pbValido;
            }
        }

        public int Archivo
        {
            get
            {
                return piCount;
            }
        }

        public int Destinos
        {
            get
            {
                return piDestinos;
            }
        }

        public int Baja()
        {
            Codigo.cRutas oRutas = new cRutas();

            return BajaArchivos(oRutas.Rutas());
        }
        //public int Baja(List<string> aDestinos)
        //{
        //    return BajaArchivos(aDestinos);
        //}

        private int BajaArchivos(List<cRutas.Ruta> aDestinos)
        {
            cBitacora oBita = new cBitacora();
            List<rDoc> rDocs = new List<rDoc>();            // Almacenar los archivos recuperados
            cMail oMail = new cMail();                      // Para enviar correos
            bool bFirst = true;

            // Obtenemos todos los documentos que no hayan sido descargados de la Base de Datos y los dejamos en la carpeta de destino
            piDestinos = 0;
            piCount = 0;

            foreach (cRutas.Ruta rDestino in aDestinos)
            {
                // Verificamos que exista la carpeta de destino
                try
                {
                    if (!Directory.Exists(rDestino.Destino))
                        Directory.CreateDirectory(rDestino.Destino);
                }
                catch (Exception ex)
                {
                    psError = ex.Message.ToString();
                    pbValido = false;
                    piCount = 0;

                    continue;                                           // Con error en el destino, pasamos al siguiente
                }

                piDestinos++;                                           // Incrementamos el contador de destinos procesados

                using (SqlConnection oDB = new SqlConnection(Properties.Settings.Default.Conexion))
                {
                    if (oDB.State == ConnectionState.Closed)
                        oDB.Open();

                    using (SqlCommand oQry = new SqlCommand("data source=10.32.37.55; initial catalog=IbdGafi; user id=sa; password=3sP3j01;", oDB))
                    {
                        oQry.CommandType = CommandType.StoredProcedure;

                        using (SqlDataReader rDatos = oQry.ExecuteReader())
                        {
                            while (rDatos.Read())
                            {
                                rDoc rDocumento = new rDoc();

                                rDocumento.Indice = rDatos.IsDBNull(rDatos.GetOrdinal("Numero")) ? 0 : rDatos.GetInt32(rDatos.GetOrdinal("Numero"));
                                rDocumento.Fecha = rDatos.IsDBNull(rDatos.GetOrdinal("Fecha")) ? DateTime.Now : rDatos.GetDateTime(rDatos.GetOrdinal("Fecha"));
                                rDocumento.Original = rDatos.IsDBNull(rDatos.GetOrdinal("Original")) ? "" : rDatos.GetString(rDatos.GetOrdinal("Original"));
                                rDocumento.Nombre = rDatos.IsDBNull(rDatos.GetOrdinal("Nombre")) ? "" : rDatos.GetString(rDatos.GetOrdinal("Nombre"));
                                rDocumento.Tipo = rDatos.IsDBNull(rDatos.GetOrdinal("Tipo")) ? "" : rDatos.GetString(rDatos.GetOrdinal("Tipo"));
                                rDocumento.Estatus = rDatos.IsDBNull(rDatos.GetOrdinal("Status")) ? 0 : rDatos.GetInt16(rDatos.GetOrdinal("Status"));
                                if (!rDatos.IsDBNull(rDatos.GetOrdinal("Contenido")))
                                    rDocumento.Contenido = (byte[])rDatos["Contenido"];

                                string sFile = "";

                                if (rDestino.ID == rDocumento.ID)
                                {
                                    try
                                    {
                                        // Grabamos el archivo
                                        sFile = Path.Combine(rDestino.Destino, rDocumento.Nombre);

                                        if (File.Exists(sFile))
                                            File.Delete(sFile);

                                        using (FileStream oFileStream = File.Create(sFile))
                                        {
                                            oFileStream.Write(rDocumento.Contenido, 0, rDocumento.Contenido.Length);
                                        }

                                        if (piDestinos == 1)
                                        {
                                            rDocs.Add(rDocumento);
                                            piCount++;                                          // Solo contamos los archivos originales
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        int iRes = oBita.AgregaBitacora(3, "BajaDocumentos", "", sFile, rDocumento.Nombre, ex.Message.ToString());
                                    }
                                }
                            }
                            if (bFirst)
                            {
                                // Solo se envía una vez el correo
                                if (oMail.EnviaCorreo("Descarga de archivos", rDestino.Destino, rDocs))
                                {
                                    bFirst = false;
                                    oBita.AgregaBitacora(0, "BajaDocumentos", "", "", "", "Se bajaron exitosamente los archivos y se envío el correo correspondiente");
                                }
                                else
                                {
                                    // Con error de correo, grabamos la bitácora y esperamos el siguiente destino
                                    oBita.AgregaBitacora(4, "BajaDocumentos", "", "", "", "Error al enviar el correo: " + oMail.Error);
                                }
                            }
                        }
                    }
                }

                // Verificamos si se copian a todos los destinos 
                if (Properties.Settings.Default.Todos == 0)
                {
                    break;                                                      // No se copian a todos los destinos, al primer válido salimos
                }
            }

            pbValido = true;

            return piCount;
        }
        private void ActualizaDoc(int iId)
        {
            using (SqlConnection oDB = new SqlConnection(Properties.Settings.Default.Conexion))
            {
                if (oDB.State == ConnectionState.Closed)
                    oDB.Open();

                using (SqlCommand oQry = new SqlCommand("[ibd].[usp_ActualizaPaseDocumento]"))
                {
                    SqlParameter oPar1 = new SqlParameter("@idDoc", iId);
                    oQry.CommandType = CommandType.StoredProcedure;
                    oQry.Connection = oDB;

                    oQry.Parameters.Add(oPar1);

                    piCount += oQry.ExecuteNonQuery();
                }
            }
        }
    }
}
